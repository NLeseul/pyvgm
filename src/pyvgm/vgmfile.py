import os
import typing

from .enums import Chip, Command

CHIP_INFO = {
    Chip.YM2413: { 'clock_offset': 0x10, 'required_version': 0x100, 'register_write_command': 0x51, 'default_clock_rate': 3579545 },
    Chip.YM2203: { 'clock_offset': 0x44, 'required_version': 0x151, 'register_write_command': 0x55, 'default_clock_rate': 4000000 },
    Chip.AY8910: { 'clock_offset': 0x74, 'required_version': 0x151, 'register_write_command': 0xa0, 'default_clock_rate': 1789772 },
    Chip.NESAPU: { 'clock_offset': 0x84, 'required_version': 0x161, 'register_write_command': 0xb4, 'default_clock_rate': 1789772 }
}

class NESAPUProcessor:
    def __init__(self):
        self._last_timer_values = [0, 0, 0]
        self._timer_values = [0, 0, 0]

    def process(self, register, value):
        if register == 0x00:
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 0, 'parameter': 'volume_mode', 'value': (value & 0x30) >> 4 }
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 0, 'parameter': 'volume', 'value': value & 0xf }
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 0, 'parameter': 'duty', 'value': value >> 6 }
        elif register == 0x02:
            self._timer_values[0] &= 0xff00
            self._timer_values[0] |= value
        elif register == 0x03:
            self._timer_values[0] &= 0xff
            self._timer_values[0] |= ((value & 0x7) << 8)
        elif register == 0x04:
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 1, 'parameter': 'volume_mode', 'value': (value & 0x30) >> 4 }
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 1, 'parameter': 'volume', 'value': value & 0xf }
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 1, 'parameter': 'duty', 'value': value >> 6 }
        elif register == 0x06:
            self._timer_values[1] &= 0xff00
            self._timer_values[1] |= value
        elif register == 0x07:
            self._timer_values[1] &= 0xff
            self._timer_values[1] |= ((value & 0x7) << 8)
        elif register == 0x0a:
            self._timer_values[2] &= 0xff00
            self._timer_values[2] |= value
        elif register == 0x0b:
            self._timer_values[2] &= 0xff
            self._timer_values[2] |= ((value & 0x7) << 8)
        elif register == 0x0c:
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 3, 'parameter': 'volume_mode', 'value': (value & 0x30) >> 4 }
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 3, 'parameter': 'volume', 'value': value & 0xf }
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 3, 'parameter': 'duty', 'value': value >> 6 }
        elif register == 0xe:
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 3, 'parameter': 'loop_noise', 'value': (value & 0x80) != 0 }
            yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': 3, 'parameter': 'noise_period', 'value': value & 0xf }
            
    def process_wait(self, ticks):
        for channel, (last, current) in enumerate(zip(self._last_timer_values, self._timer_values)):
            #print(channel, last, current)
            if last != current:
                new_frequency = 111860.8 / ((2 * current) + 1)
                if channel == 2:
                    new_frequency /= 2
                yield { 'command': Command.Parameter, 'chip': Chip.NESAPU, 'channel': channel, 'parameter': 'frequency', 'value': new_frequency }
        self._last_timer_values = self._timer_values.copy()


class AY8910Processor:
    def __init__(self):
        self._last_timer_values = [0, 0, 0]
        self._timer_values = [0, 0, 0]

    def process(self, register, value):
        if register == 0x00:
            self._timer_values[0] &= 0xff00
            self._timer_values[0] |= value
        elif register == 0x01:
            self._timer_values[0] &= 0xff
            self._timer_values[0] |= ((value & 0xf) << 8)
        elif register == 0x02:
            self._timer_values[1] &= 0xff00
            self._timer_values[1] |= value
        elif register == 0x03:
            self._timer_values[1] &= 0xff
            self._timer_values[1] |= ((value & 0xf) << 8)
        elif register == 0x04:
            self._timer_values[2] &= 0xff00
            self._timer_values[2] |= value
        elif register == 0x05:
            self._timer_values[2] &= 0xff
            self._timer_values[2] |= ((value & 0xf) << 8)
        elif register == 0x08:
            yield { 'command': Command.Parameter, 'chip': Chip.AY8910, 'channel': 0, 'parameter': 'volume_mode', 'value': (value & 0x10) >> 4 }
            yield { 'command': Command.Parameter, 'chip': Chip.AY8910, 'channel': 0, 'parameter': 'volume', 'value': value & 0xf }
        elif register == 0x09:
            yield { 'command': Command.Parameter, 'chip': Chip.AY8910, 'channel': 1, 'parameter': 'volume_mode', 'value': (value & 0x10) >> 4 }
            yield { 'command': Command.Parameter, 'chip': Chip.AY8910, 'channel': 1, 'parameter': 'volume', 'value': value & 0xf }
        elif register == 0x0a:
            yield { 'command': Command.Parameter, 'chip': Chip.AY8910, 'channel': 2, 'parameter': 'volume_mode', 'value': (value & 0x10) >> 4 }
            yield { 'command': Command.Parameter, 'chip': Chip.AY8910, 'channel': 2, 'parameter': 'volume', 'value': value & 0xf }
            
    def process_wait(self, ticks):
        for channel, (last, current) in enumerate(zip(self._last_timer_values, self._timer_values)):
            if last != current:
                new_frequency = 1789772.5 / (16 * current)
                yield { 'command': Command.Parameter, 'chip': Chip.AY8910, 'channel': channel, 'parameter': 'frequency', 'value': new_frequency }
        self._last_timer_values = self._timer_values.copy()


class RawCommand:
    def __init__(self):
        pass
        
    @property
    def data(self) -> bytes:
        raise Exception("Not implemented")
    

class WaitCommand(RawCommand):
    def __init__(self, ticks:int):
        RawCommand()
        self._ticks = ticks
        
    def __str__(self) -> str:
        return f"Wait({self.ticks})"
        
    @property
    def ticks(self) -> int:
        return self._ticks
        
    @property
    def data(self) -> bytes:
        if self._ticks == 735:
            return b'\x62'
        elif self._ticks == 882:
            return b'\x63'
        elif self._ticks > 0 and self._ticks < 16:
            return ((self._ticks - 1) | 0x70).to_bytes(1, byteorder='little')
        else:
            return b'\x61' + self._ticks.to_bytes(2, byteorder='little')
    
       
class WriteRegisterCommand(RawCommand):
    def __init__(self, chip:Chip, register:int, value:int):
        self._chip = chip
        self._register = register
        self._value = value
        
        self._write_command = CHIP_INFO[self._chip]['register_write_command']
        
    def __str__(self) -> str:
        return f"WriteRegister({self._register:02x}, {self._value:02x}) - {self._chip}"
    
    @property
    def chip(self) -> Chip:
        return self._chip
    
    @property
    def register(self) -> int:
        return self._register
    
    @property
    def value(self) -> int:
        return self._value
    
    @property
    def data(self) -> bytes:
        return self._write_command.to_bytes(1, byteorder='little') +\
            self._register.to_bytes(1, byteorder='little') +\
            self._value.to_bytes(1, byteorder='little')


class PCMDataCommand(RawCommand):
    def __init__(self, pcm_type, pcm_data):
        self._pcm_type = pcm_type
        self._pcm_data = pcm_data
        
    @property
    def data(self) -> bytes:
        return b'\x67\x66' + self._pcm_type.to_bytes(1, byteorder='little') +\
            len(self._pcm_data).to_bytes(4, byteorder='little') +\
            self._pcm_data


class PCMRamWriteCommand(RawCommand):
    def __init__(self, pcm_type, read_addr, write_addr, length):
        self._pcm_type = pcm_type
        self._read_addr = read_addr
        self._write_addr = write_addr
        self._length = length
        
    @property
    def data(self) -> bytes:
        return b'\x68\x66' + self._pcm_type.to_bytes(1, byteorder='little') +\
            self._read_addr.to_bytes(3, byteorder='little') +\
            self._write_addr.to_bytes(3, byteorder='little') +\
            (self._length.to_bytes(3, byteorder='little') if self._length > 0x01000000 else b'\x00\x00\x00')
    

class VGMFile:
    def __init__(self):
        self._commands = []
        self._chips = {}
        
        self._total_duration = 0

        self._track_name = None
        self._original_track_name = None
        self._game_name = None
        self._original_game_name = None
        self._system_name = None
        self._original_system_name = None
        self._composer_name = None
        self._original_composer_name = None
        self._game_release_date = None
        self._converter_name = None
        self._notes = None

    def load(input:typing.BinaryIO):

        vgm = VGMFile()

        start_pos = input.tell()

        header_bytes = input.read(0x40)

        if len(header_bytes) < 0x40 or header_bytes[0x00:0x04] != b'Vgm ':
            raise Exception("Not a valid VGM file.")

        vgm._file_length = int.from_bytes(header_bytes[0x04:0x08], byteorder='little') + 4
        #print(f"Length: {vgm._file_length}")

        vgm._version = int.from_bytes(header_bytes[0x08:0x0c], byteorder='little')
        #print(f"Version: {vgm._version:08x}")

        gd3_pos = int.from_bytes(header_bytes[0x14:0x18], byteorder='little') + 0x14

        total_samples = int.from_bytes(header_bytes[0x18:0x1c], byteorder='little')
        loop_offset = int.from_bytes(header_bytes[0x1c:0x20], byteorder='little')
        loop_samples = int.from_bytes(header_bytes[0x20:0x24], byteorder='little')

        #print(f"Total samples: {vgm._total_samples}, loop samples: {vgm._loop_samples}, loop offset: {vgm._loop_offset}")
        #print(f"GD3 offset: {gd3_pos}")

        if vgm._version >= 0x101:
            sample_rate = int.from_bytes(header_bytes[0x24:0x28], byteorder='little')
            #print(f"Sample rate: {sample_rate}")

        if vgm._version >= 0x150:
            data_pos = int.from_bytes(header_bytes[0x34:0x38], byteorder='little') + 0x34
            #print(f"Data offset: {data_pos}")

            if data_pos > 0x40:
                header_bytes += input.read(data_pos - 0x40)
                #print(f" Total header length: {len(header_bytes)}")
        else:
            data_pos = 0x40


        vgm._chips = {}
        for chip, chip_info in CHIP_INFO.items():
            if vgm._version >= chip_info['required_version']:
                clock_rate = int.from_bytes(header_bytes[chip_info['clock_offset']:chip_info['clock_offset'] + 4], byteorder='little')
                if clock_rate > 0:
                    #print(f"Clock rate for chip {chip} is {clock_rate}")
                    if chip == Chip.NESAPU and (clock_rate & 0x80000000) != 0:
                        vgm._chips[chip] = { 'clock_rate': clock_rate & 0x7fffffff, 'enable_fds': True }
                    else:
                        vgm._chips[chip] = { 'clock_rate': clock_rate }

        
        if vgm._version >= 0x170:
            extra_header_offset = int.from_bytes(header_bytes[0xbc:0xc0], byteorder='little')
        else:
            extra_header_offset = 0
        if extra_header_offset > 0:
            extra_header_offset += 0xbc
        #print(f"Extra header offset: {extra_header_offset}")

        input.seek(start_pos, os.SEEK_SET)
        input.seek(data_pos, os.SEEK_CUR)

        vgm._commands = []
        while True:
            command = VGMFile.command_from_bytes(input)
            if command is None:
                break
            else:
                vgm.add_command(command)

        if vgm.total_duration != total_samples:
            raise Exception(f"Sample count mismatch - {vgm.total_duration} vs {total_samples}")

        input.seek(start_pos, os.SEEK_SET)
        input.seek(gd3_pos, os.SEEK_CUR)

        if input.read(4) != b'Gd3 ' or input.read(4) != b'\x00\x01\x00\x00':
            raise Exception("Invalid GD3 block.")
        input.read(4) # GD3 data length

        vgm._track_name = VGMFile._read_gd3_string(input)
        vgm._original_track_name = VGMFile._read_gd3_string(input)
        vgm._game_name = VGMFile._read_gd3_string(input)
        vgm._original_game_name = VGMFile._read_gd3_string(input)
        vgm._system_name = VGMFile._read_gd3_string(input)
        vgm._original_system_name = VGMFile._read_gd3_string(input)
        vgm._composer_name = VGMFile._read_gd3_string(input)
        vgm._original_composer_name = VGMFile._read_gd3_string(input)
        vgm._game_release_date = VGMFile._read_gd3_string(input)
        vgm._converter_name = VGMFile._read_gd3_string(input)
        vgm._notes = VGMFile._read_gd3_string(input)


        return vgm
    

    def save(self, output:typing.BinaryIO):
        
        header_bytes = bytearray(256)
        data_bytes = bytearray()
        gd3_bytes = bytearray()

        sample_count = 0
        for command in self.commands:
            if isinstance(command, WaitCommand):
                sample_count += command.ticks
            data_bytes += command.data
        data_bytes += b'\x66'

        if self.total_duration != sample_count:
            raise Exception(f"Sample count mismatch - {self.total_duration} vs {sample_count}")
            
        gd3_bytes += b'Gd3 '
        gd3_bytes += 0x100.to_bytes(4, byteorder='little')
        gd3_bytes += 0x0.to_bytes(4, byteorder='little') # Length placeholder
        gd3_bytes += VGMFile._encode_gd3_string(self.track_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.original_track_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.game_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.original_game_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.system_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.original_system_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.composer_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.original_composer_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.game_release_date)
        gd3_bytes += VGMFile._encode_gd3_string(self.converter_name)
        gd3_bytes += VGMFile._encode_gd3_string(self.notes)
        gd3_bytes[0x08:0x0c] = (len(gd3_bytes) - 0xc).to_bytes(4, byteorder='little')
            
        header_bytes[0x00:0x04] = b'Vgm '
        header_bytes[0x04:0x08] = (len(data_bytes) + len(header_bytes) + len(gd3_bytes) - 4).to_bytes(4, byteorder='little')
        header_bytes[0x08:0x0c] = 0x171.to_bytes(4, byteorder='little')
        
        header_bytes[0x14:0x18] = (len(data_bytes) + len(header_bytes) - 0x14).to_bytes(4, byteorder='little')
        header_bytes[0x18:0x1c] = sample_count.to_bytes(4, byteorder='little')
        
        header_bytes[0x34:0x38] = (len(header_bytes) - 0x34).to_bytes(4, byteorder='little')
        
        for chip, chip_info in self._chips.items():
            offset = CHIP_INFO[chip]['clock_offset']
            clock = chip_info['clock_rate']
            
            header_value = clock
            
            if 'enable_fds' in chip_info and chip_info['enable_fds'] == True:
                header_value |= 0x80000000
            
            header_bytes[offset:offset+4] = header_value.to_bytes(4, byteorder='little')
            
        output.write(header_bytes)
        output.write(data_bytes)
        output.write(gd3_bytes)


    def _read_gd3_string(input) -> str:
        result_bytes = b''
        while True:
            char_bytes = input.read(2)
            if char_bytes == b'\x00\x00':
                break
            result_bytes += char_bytes
        return result_bytes.decode('utf16')

    def _encode_gd3_string(string:str) -> bytes:
        encoded = string.encode('utf16') if string is not None else b''
        return encoded[2:] + b'\x00\x00'
    

    @property
    def track_name(self) -> str:
        return self._track_name
    @track_name.setter
    def track_name(self, value:str) -> None:
        self._track_name = value

    @property
    def game_name(self) -> str:
        return self._game_name
    @game_name.setter
    def game_name(self, value:str) -> None:
        self._game_name = value

    @property
    def system_name(self) -> str:
        return self._system_name
    @system_name.setter
    def system_name(self, value:str) -> None:
        self._system_name = value

    @property
    def composer_name(self) -> str:
        return self._composer_name
    @composer_name.setter
    def composer_name(self, value:str) -> None:
        self._composer_name = value

    @property
    def original_track_name(self) -> str:
        return self._original_track_name
    @original_track_name.setter
    def original_track_name(self, value:str) -> None:
        self._original_track_name = value

    @property
    def original_game_name(self) -> str:
        return self._original_game_name
    @original_game_name.setter
    def original_track_name(self, value:str) -> None:
        self._original_game_name = value

    @property
    def original_system_name(self) -> str:
        return self._original_system_name
    @original_system_name.setter
    def original_system_name(self, value:str) -> None:
        self._original_system_name = value

    @property
    def original_composer_name(self) -> str:
        return self._original_composer_name
    @original_composer_name.setter
    def original_composer_name(self, value:str) -> None:
        self._original_composer_name = value

    @property
    def game_release_date(self) -> str:
        return self._game_release_date
    @game_release_date.setter
    def game_release_date(self, value:str) -> None:
        self._game_release_date = value

    @property
    def converter_name(self) -> str:
        return self._converter_name
    @converter_name.setter
    def converter_name(self, value:str) -> None:
        self._converter_name = value

    @property
    def notes(self) -> str:
        return self._notes
    @notes.setter
    def notes(self, value:str) -> None:
        self._notes = value

    @property
    def total_duration(self) -> int:
        return self._total_duration

    @property
    def commands(self):
        yield from self._commands

    @property
    def processed_commands(self):

        nes_processor = NESAPUProcessor()
        ay8310_processor = AY8910Processor()

        for c in self._commands:
            if c['command'] == Command.Wait:
                yield from nes_processor.process_wait(c['wait_ticks'])
                yield from ay8310_processor.process_wait(c['wait_ticks'])
                yield c
            elif c['command'] == Command.RegisterWrite:
                if c['chip'] == Chip.NESAPU:
                    yield from nes_processor.process(c['register'], c['value'])
                elif c['chip'] == Chip.AY8910:
                    yield from ay8310_processor.process(c['register'], c['value'])
                else:
                    raise Exception(f"No processing is supported for chip {c['chip']}.")
            else:
                raise Exception(f"Unknown command {c['command']}!")
            
    def add_chip(self, chip:Chip, clock_rate:int = None) -> None:
        if clock_rate is None:
            clock_rate = CHIP_INFO[chip]['default_clock_rate']
            
        self._chips[chip] = { 'clock_rate': clock_rate }
        
    def add_command(self, command:Command) -> None:
        self._commands.append(command)
        
        if isinstance(command, WaitCommand):
            self._total_duration += command.ticks

        if isinstance(command, WriteRegisterCommand):
            if command.chip not in self._chips:
                self.add_chip(command.chip)
                
            # Special case - Add a flag for FDS registers
            if command.chip == Chip.NESAPU and command.register == 0x23:
                self._chips[command.chip]['enable_fds'] = True
            
    def command_from_bytes(input:typing.BinaryIO) -> Command:
        read_bytes = input.read(1)
        if len(read_bytes) < 1:
            return None

        command_id = read_bytes[0]
        if command_id == 0x66:
            return None
            

        elif command_id >= 0x70 and command_id <= 0x7f:
            # Wait, half-byte
            duration = (command_id & 0xf) + 1
            return WaitCommand(duration)
        elif command_id == 0x61:
            # Wait, two bytes
            duration = int.from_bytes(input.read(2), byteorder='little')
            return WaitCommand(duration)
        elif command_id == 0x62:
            # Wait, NTSC frame
            return WaitCommand(735)
        elif command_id == 0x63:
            # Wait, PAL frame
            return WaitCommand(882)

        elif command_id == 0x67:
            # PCM data
            input.read(1) # Compatibility guard
            data_type = int.from_bytes(input.read(1), byteorder='little')
            data_length = int.from_bytes(input.read(4), byteorder='little')

            data = input.read(data_length) # Data
            return PCMDataCommand(data_type, data)
        
        elif command_id == 0x68:
            # PCM RAM write
            input.read(1) # Compatibility guard
            data_type = int.from_bytes(input.read(1), byteorder='little')
            read_offset = int.from_bytes(input.read(3), byteorder='little')
            write_offset = int.from_bytes(input.read(3), byteorder='little')
            data_length = int.from_bytes(input.read(3), byteorder='little')
            
            if data_length == 0:
                data_length = 0x01000000
                
            return PCMRamWriteCommand(data_type, read_offset, write_offset, data_length)

        else:
            chip_found = False
            for chip, chip_info in CHIP_INFO.items():
                if chip_info['register_write_command'] == command_id:
                    chip_found = True
                        
                    register = input.read(1)[0]
                    value = input.read(1)[0]
                    return WriteRegisterCommand(chip, register, value)
                        
            if not chip_found:
                raise Exception(f"Unknown command ID {command_id:02x} at position {input.tell() - 1:x}!")
            
