from pyvgm import Command

class ToNotesProcessor:
    def process(command_list):
        current_tick = 0
        for command in command_list:
            if command['command'] == Command.Wait:
                current_tick += command['wait_ticks']
                print(current_tick)