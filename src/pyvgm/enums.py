from enum import Enum, auto

class Chip(Enum):
    YM2413 = 2,
    YM2203 = 6,
    AY8910 = 18,
    NESAPU = 20,

class Command(Enum):
    Wait = auto(),
    RegisterWrite = auto(),
    Parameter = auto()

